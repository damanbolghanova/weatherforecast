package com.example.weatherforecast.models;

public class Sys {
    private Integer type;
    private Double message;
    private Integer sunrise;
    private Integer sunset;
}
